
using CommandsService.Models;
using CommandsService.SyncDataServices.Grpc;

namespace CommandsService.Data
{
    public static class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var commandRepo = serviceScope.ServiceProvider.GetRequiredService<ICommandRepo>();
                var grpcClient = serviceScope.ServiceProvider.GetService<IPlatformDataClient>();
                var platforms = grpcClient.ReturnAllPlatforms();
                SeedData(commandRepo, platforms);
            }
        }

        private static void SeedData(ICommandRepo commandRepo, IEnumerable<Platform> platforms)
        {
            Console.WriteLine("---> Seeding new platforms...");
            foreach (var plat in platforms)
            {
                if (!commandRepo.ExternalPlatformExist(plat.ExternalID))
                {
                    commandRepo.CreatePlatform(plat);
                }
            }

            commandRepo.SaveChanges();
        }
    }

}