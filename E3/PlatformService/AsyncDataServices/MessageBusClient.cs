
using System.Text;
using System.Text.Json;
using PlatformService.Dtos;
using RabbitMQ.Client;

namespace PlatformService.AsyncDataServices
{
    public class MessageBusClient : IMessageBusClient
    {
        private readonly IConfiguration _configuration;
        private IConnection _conn;
        private readonly IModel _channel;

        public MessageBusClient(IConfiguration configuration)
        {
            this._configuration = configuration;
            var factory = new RabbitMQ.Client.ConnectionFactory() { HostName = _configuration["RabbitMQHost"], Port = int.Parse(_configuration["RabbitMQPort"]) };

            try
            {
                _conn = factory.CreateConnection();
                _channel = _conn.CreateModel();
                _channel.ExchangeDeclare(exchange: "trigger", type: ExchangeType.Fanout);

                _conn.ConnectionShutdown += ConnectionShutdown;
                Console.WriteLine($"---> connected to message bus");
            }
            catch (System.Exception ex)
            {
                Console.WriteLine($"---> Could not connect to rabbitmq message bus, error message: {ex.Message}");
            }
        }
        public void PublishNewPlatform(PlatformPublishedDto platformPublishedDto)
        {
            var message = JsonSerializer.Serialize(platformPublishedDto);
            if (_conn.IsOpen)
            {
                Console.WriteLine("---> rabbitmq connection open, sending message...");
                SendMessage(message);
            }
            else
            {
                Console.WriteLine("---> rabbitmq connection closed, not sending message...");
            }
        }

        private void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "trigger", routingKey: "", basicProperties: null, body: body);

            Console.WriteLine($"---> we have sent {message}");
        }

        private void ConnectionShutdown(object sender, ShutdownEventArgs args)
        {
            Console.WriteLine("---> rabbitmq connection shutdown");
        }

        public void Dispose()
        {
            Console.WriteLine("MessageBus Disposed");

            if (_channel.IsOpen)
            {
                _channel.Close();
                _conn.Close();
            }
        }
    }

}