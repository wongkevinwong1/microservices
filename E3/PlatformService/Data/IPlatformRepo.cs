using PlatformService.Models;

namespace PlatformService.Data{

    public interface IPlatformRepo{

        IEnumerable<Platform> GetAllPlatforms();

        void Create(Platform plat);

        Platform GetPlatformById(int id);

        bool SaveChanges();
    }
}