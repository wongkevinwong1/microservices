using Microsoft.EntityFrameworkCore;
using PlatformService.Models;

namespace PlatformService.Data
{

    public static class Prepdb
    {

        public static void PrepData(WebApplicationBuilder appBuilder, bool isProduction)
        {
            var sp = appBuilder.Services.BuildServiceProvider();
            var getserviceAppDbContext = sp.GetService<AppDbContext>();
            if (getserviceAppDbContext == null)
            {
                Console.WriteLine("---> getserviceAppDbContext object is null");
            }
            SeedData(sp.GetService<AppDbContext>(), isProduction);
            // using (var serviceScope = appBuilder.Services.BuildServiceProvider().gets.CreateScope())
            // {
            //     SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>());
            // }
        }

        private static void SeedData(AppDbContext context, bool isProduction)
        {
            if (isProduction)
            {
                Console.WriteLine("---> attempting to apply migrations...");
                try
                {
                    context.Database.Migrate();
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine($"---> could not run migrations : {ex.Message}");
                }
            }
            if (!context.Platforms.Any())
            {

                Console.WriteLine("---> seed data");
                context.Platforms.AddRange(
                    new Platform() { Name = "Dot Net", Publisher = "Microsoft", Cost = "Free" },
                    new Platform() { Name = "SQL Server Express", Publisher = "Microsoft", Cost = "Free" },
                    new Platform() { Name = "Kubernetes", Publisher = "Cloud Native Computing Foundation", Cost = "Free" }
                );

                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("---> already have initial data");
            }
        }
    }
}