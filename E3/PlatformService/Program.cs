//https://anthonygiretti.com/2021/08/12/asp-net-core-6-working-with-minimal-apis/

using Microsoft.EntityFrameworkCore;
using PlatformService.AsyncDataServices;
using PlatformService.Data;
using PlatformService.SyncDataServices.Grpc;
using PlatformService.SyncDataServices.Http;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.\
builder.Configuration.AddJsonFile("appSettings.json", true, true);
builder.Configuration.AddJsonFile($"appSettings.{builder.Environment.EnvironmentName}.json", true, true);
builder.Services.AddHttpClient<ICommandDataClient, CommandDataClient>();


if (builder.Environment.IsDevelopment())
{
    Console.WriteLine("---> use in memory database");
    builder.Services.AddDbContext<AppDbContext>(opt => opt.UseInMemoryDatabase("InMem"));
}
else
{
    Console.WriteLine("---> use sql server");
    Console.WriteLine($"---> value of db conn string: {builder.Configuration.GetConnectionString("PlatformsConn")}");
    builder.Services.AddDbContext<AppDbContext>(opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("PlatformsConn")));
}

builder.Services.AddScoped<IPlatformRepo, PlatformRepo>();
builder.Services.AddSingleton<IMessageBusClient, MessageBusClient>();

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddGrpc();
var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGrpcService<GrpcPlatformService>();
app.MapGet("/Protos/Platforms.proto", async context =>
    {
        await context.Response.WriteAsync(File.ReadAllText("Protos/Platforms.proto"));
    });

Prepdb.PrepData(builder, app.Environment.IsProduction());

Console.WriteLine($"---> CommandService Endpoint {builder.Configuration["CommandService"]}");

app.Run();
