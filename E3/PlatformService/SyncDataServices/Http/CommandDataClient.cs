

using System.Text;
using System.Text.Json;
using PlatformService.Dtos;
using PlatformService.SyncDataServices.Http;

namespace PlatformService.SyncDataServices.Http
{
    public class CommandDataClient : ICommandDataClient
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public CommandDataClient(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        public async Task SendPlatformToCommand(PlatformReadDto plat)
        {
            var httpContent = new StringContent(JsonSerializer.Serialize(plat), Encoding.UTF8, "application/json");


            var result = await _httpClient.PostAsync($"{_configuration["CommandsService"]}", httpContent);

            if (result.IsSuccessStatusCode)
            {
                Console.WriteLine("---> Sync POST to CommandService was OK");
            }
            else
            {
                Console.WriteLine("---> Sync POST to CommandService was NOT OK");
            }
        }
    }
}